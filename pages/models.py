from django.conf import settings
from django.db import models


# Create your models here.
class Offer(models.Model):
    name = models.CharField(max_length=254)
    date = models.CharField(default=00000000, max_length=8)
    description = models.TextField(blank=True)
    guardian = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="guardian_offers", on_delete=models.CASCADE)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="owner_offers", on_delete=models.CASCADE)


class Plant(models.Model):
    name = models.CharField(max_length=254)
    thumbnail = models.TextField()
    offers = models.ManyToManyField(Offer)
    description = models.TextField(blank=True)


class Advice(models.Model):
    content = models.TextField(blank=True)
    date = models.CharField(default=00000000, max_length=8)
    poster = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)


class Picture(models.Model):
    name = models.CharField(max_length=128)
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)


class Comment(models.Model):
    content = models.TextField(blank=True)
    date = models.CharField(default=00000000, max_length=8)
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)

