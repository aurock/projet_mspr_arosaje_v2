from django.shortcuts import render
from datetime import datetime   
from .models import Plant
from .models import Advice
from .models import Offer
from .forms import AddOfferForm
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
import os

from pages.models import Offer, Plant, Advice


def index(request):
    #? A commenter (pour ajouter a la base)
    try:
        user = User.objects.get(username='test')
    except User.DoesNotExist:
        user = User.objects.create_user(username='test', email='bjr@example.com', password='test')
        user.save()
    all_plants = Plant.objects.all()
    all_advices = Advice.objects.all()
    for plant in all_plants:
        plant.delete()
    for advice in all_advices:
        advice.delete()
    plantes = []
    advices = []
    plante = Plant(name='Orchidée', thumbnail='https://photos.gammvert.fr/v5/products/fiche_590/56490-orchidee-phalaenopsis-2-tiges-blanche-2.jpg', description='De la famille des orchidacées et en provenance d\'Asie, l\'orchidée est une plante d\'intérieur tropicale et subtropicale dont l\'origine étymologique vient du mot latin orchis, lui-même emprunté au grec orchidion, en français testicule, en référence à la forme de certaines racines protubérantes.')
    advices.append(Advice(content='Elles n\'apprécient pas de vivre en pots, elles doivent être en suspension, dans un endroit lumineux, protégé du froid. Il faut laisser leurs racines aériennes, parfois très longues, pendre dans le vide en cascade. Il est conseillé de les tremper régulièrement dans une eau tiède et peu calcaire.',date='21/03/2023',poster=user,plant=plante))
    plantes.append(plante)
    plantes.append(Plant(name='Ficus', thumbnail='https://photos.gammvert.fr/v5/products/fiche_590/9880-ficus-golden-king.jpg', description='Ficus est un genre dans la famille des Moraceae, représenté par des arbres, des arbustes ou des lianes. Avec plus de 750 espèces connues il s\'agit, avec Dorstenia, d\'un des principaux genres de sa famille.'))
    plantes.append(Plant(name='Palmier', thumbnail='https://photos.gammvert.fr/v5/products/fiche_590/19201-palmier-nain-chamaedora-hauteur-65cm.jpg', description='Arbre monocotylédone à tronc (stipe) peu ou pas ramifié, à frondaison sommitale formée de feuilles composées, qui sont pennées ou palmées selon l\'espèce. (La famille des palmiers, ou palmacées, compte plus de 4 000 espèces.)'))
    plantes.append(Plant(name='Anthurium', thumbnail='https://photos.gammvert.fr/v5/products/fiche_590/56475-anthurium-rouge-nevada-2.jpg', description='L\'anthurium est une plante épiphyte d\'origine tropicale de la famille des aracées. L\'espèce type provient des forêts de Colombie. On trouve dans le commerce (jardineries, fleuristes) des hybrides aux grandes fleurs rouges, roses ou blanches.'))
    plantes.append(Plant(name='Citronnier', thumbnail='https://photos.gammvert.fr/v5/products/fiche_590/41914-citronnier-des-4-saisons-5.jpg', description='Le citronnier est un arbuste vigoureux aux branches robustes et épineuses. Les feuilles alternes et coriaces sont grandes et très parfumées. Les fleurs sont blanches et peu odorantes, regroupées à l\'aisselle des feuilles. Les fruits sont des baies ovales, jaune vif, avec un mamelon au sommet.'))
    for plante in plantes:
        plante.save()
    for advice in advices:
        advice.save()
        
    all_offers = Offer.objects.all()
    all_plants = Plant.objects.all()
    context = {
        'offers': all_offers,
        'plants': all_plants
    }
    return render(request, 'pages/index.html', context)


def login(request):
    return render(request, 'pages/login.html')


def plant_list(request):
    all_plants = Plant.objects.all()
    return render(request, 'pages/plant_list.html',{'all_plants': all_plants})

def plant_detail(request, plant_id):
    plante = Plant.objects.get(id=plant_id)
    advices = Advice.objects.filter(plant=plante)
    return render(request, 'pages/plant_detail.html',{'plante' : plante, 'advices': advices})

def offer_list(request):
    all_offers = Offer.objects.all()
    context = {
        'offers': all_offers
    }
    return render(request, 'pages/offer_list.html', context)

def offer_detail(request, offer_id):
    offer = Offer.objects.get(id=offer_id)
    user = User.objects.get(id=offer.owner_id)
    context = {
        'offer': offer,
        'user': user
    }
    return render(request, 'pages/offer_detail.html', context)

def offer_add(request):
    # Users nécessaires :
    try:
        gardien = User.objects.get(username='Gardien')
    except User.DoesNotExist:
        gardien = User.objects.create_user(username='Gardien', email='jardinage@example.com', password='gazon')
        gardien.save()
    try:
        jm = User.objects.get(username='Jean Margaux')
    except User.DoesNotExist:
        jm = User.objects.create_user(username='Jean Margaux', email='potajet@example.com', password='pelouz')
        jm.save()
    # Traitement formulaire :
    if request.method == 'POST':
        fs = FileSystemStorage()
        titre = request.POST.get('titre')
        desc = request.POST.get('description')

        offer = Offer(name=titre,date=datetime.now(),description=desc,guardian=gardien,owner=jm)
        offer.save()
        file = request.FILES['photo']
        nom_fichier = fs.save(os.path.join('images', file.name), file)
        os.rename(nom_fichier,'images/offer_'+str(offer.id)+'.jpg')
        return render(request, 'pages/offer_list.html')
    # Envoie formulaire :
    form = AddOfferForm()
    return render(request, 'pages/offer_add.html', {'form': form})

def user_detail(request, user_id):
    print(user_id)
    user = User.objects.get(id=user_id)
    print(f'User : { user }')

    offer = Offer.objects.get(owner=user_id)
    print(f'Offer : { offer }')
        
    context = {
        'user': user,
        'offer': offer
    }
    return render(request, 'pages/user_detail.html', context)