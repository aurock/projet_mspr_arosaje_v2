from django.contrib import admin

from pages.models import Offer, Plant, Advice, Picture, Comment

# Register your models here.
admin.site.register(Offer)
admin.site.register(Plant)
admin.site.register(Advice)
admin.site.register(Picture)
admin.site.register(Comment)
